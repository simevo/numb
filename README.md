numb - C++ program to convert integers into words
=================================================

The program reads text lines from stdin, each with a positive decimal number.
Numbers up to 9 digits are then spelled out in English.

# Build

Build with:

    g++ -o numb numb.cc

# Usage

Sample use:

    $ echo 0 | ./numb
    zero
    $ echo 999999999999 | ./numb 
    nine hundred ninety nine billion nine hundred ninety nine million nine hundred ninety nine thousand and nine hundred ninety nine 

To enjoy the **Desk calculator demo** from https://github.com/susam/tucl/blob/master/the-unix-command-language.md on Debian 11 (bullseye):

    $ sudo apt install espeak-ng-espeak dc
    $ dc | ./numb | speak
    
then type this followed by ENTER:

    2 32 ^ p

# TODO

Long int handles up to 9 digits, switch to unsigned long long int to handle more digits.

# Copyright and License

Based on https://www.geeksforgeeks.org/program-to-convert-a-given-number-to-words-set-2/

Modifications Copyright (C) 2020 Paolo Greppi <paolo.greppi@libpf.com>

Licensed under the GNU GENERAL PUBLIC LICENSE Version 3.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
