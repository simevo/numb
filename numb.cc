// numb - C++ program to convert integers into words
//
// The program reads text lines from stdin, each with a positive decimal number.
// Numbers up to 9 digits are then spelled out in English.
//
// Based on https://www.geeksforgeeks.org/program-to-convert-a-given-number-to-words-set-2/
//
// Modifications Copyright (C) 2020 Paolo Greppi <paolo.greppi@libpf.com>
//
// Licensed under the GNU GENERAL PUBLIC LICENSE Version 3.
//
// build with:
//   g++ -o numb numb.cc
//
// Repo: https://gitlab.com/simevo/numb

#include <iostream>
#include <string>

// strings at index 0 is not used, it is to make array indexing simple 
std::string one[] = { "", "one ", "two ", "three ", "four ",  "five ", "six ", "seven ", "eight ",  "nine ", "ten ",
  "eleven ", "twelve ", "thirteen ", "fourteen ", "fifteen ", "sixteen ", "seventeen ", "eighteen ", "nineteen " };

// strings at index 0 and 1 are not used, they are there to 
// make array indexing simple 
std::string ten[] = { "", "", "twenty ", "thirty ", "forty ",  "fifty ", "sixty ", "seventy ", "eighty ",  "ninety " };

// n is 1- or 3-digit number 
std::string numToWords(int n, std::string s) {
  std::string str = "";
  // handles digit at hundreds places (if any) 
  if (n / 100 > 0) {
    str += one[(n / 100) % 10];
    str += "hundred ";
  }

  // if n is more than 19, divide it 
  if (n % 100 > 19)
    str += ten[(n % 100) / 10] + one[n % 10];
  else
    str += one[n % 100];

  // if n is non-zero 
  if (n)
    str += s;

  return str;
} 

// Function to print a given number in words 
std::string convertToWords(long n) { 
  // stores word representation of given number n 
  std::string out;

  // handles digits at billions places (if any) 
  out += numToWords((n / 1000000000), "billion ");

  // handles digits at millions places (if any) 
  out += numToWords(((n / 1000000) % 1000), "million ");

  // handles digits at thousands places (if any) 
  out += numToWords(((n / 1000) % 1000), "thousand ");

  if (n > 1000 && n % 1000) {
    out += "and ";
  }

  // handles digits at ones, tens and hundred places (if any) 
  out += numToWords((n % 1000), "");

  // handles zero
  if(out=="") {
    out = "zero";
  }
  return out;
} 

// Driver code 
int main(int argc, char *argv[]) { 
  std::string line;
  while(getline(std::cin, line)) {
    long n = atol(line.c_str());
    std::cout << convertToWords(n) << std::endl;
  }
  return 0;
} 
